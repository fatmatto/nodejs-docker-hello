
const http = require('http')
const packageJson = require('./package.json')
const app = http.createServer((request, response) => {
  response.writeHead(200, { 'Content-Type': 'text/plain' })
  response.write(`Hello from version ${packageJson.version}\n`)
  response.write(`Time is ${new Date()}`)
  response.end()
})

app.listen(3000)
